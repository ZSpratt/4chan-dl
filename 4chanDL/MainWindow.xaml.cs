﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Diagnostics;
using System.Xml;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.IO;
using System.Windows.Forms;

namespace _4chanDL {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		public string threadPath = "";
		public string date;

		public static float volume = 1.0f;
		public static bool mute = false;
		public static bool FFMPEGLoaded = false;

		public MainWindow() {
			InitializeComponent();
			if (Properties.Settings.Default.RootFolder == "") {
				Properties.Settings.Default.RootFolder = Directory.GetCurrentDirectory();
			}
			if (Properties.Settings.Default.FFMPEG != "") {
				FFMPEGLoaded = true;
			}
			volume = Properties.Settings.Default.Volume;
			mute = Properties.Settings.Default.Mute;
			volumeSlider.Value = volume;
			muteBox.IsChecked = mute;
			ffmpegAddress.Text = Properties.Settings.Default.FFMPEG;
			diskAddress.Text = Properties.Settings.Default.RootFolder;

			date = DateTime.Today.Year.ToString("0000") + "-" + DateTime.Today.Month.ToString("00") + "-" + DateTime.Today.Day.ToString("00");
			Debug.Print(date);
		}

		private void AddTab_Click(object sender, RoutedEventArgs e) {
			string dest = addressBox.Text;

			if (dest == "") {
				return;
			}

			for (int i = 0; i < threadTabs.Items.Count; i++) {
				if (((threadTabs.Items[i] as TabItem).Content as ThreadTab).boardAddress.Equals(dest)) {
					threadTabs.SelectedIndex = i;
					return;
				}
			}

			var page = new TabItem();
			ThreadTab uc = new ThreadTab(addressBox.Text, date, diskAddress.Text);
			page.Content = uc;
			page.Header = uc.title.Text + "(" + uc.boardTitle + ")";
			uc.title.TextChanged += (u, args) => { page.Header = uc.title.Text + " (" + uc.boardTitle + ")"; };
			//uc.downloadButton_Click(null, null);
			threadTabs.Items.Add(page);
			threadTabs.SelectedIndex = threadTabs.Items.Count - 1;
			addressBox.Text = "";
		}

		private void DelTab_Click(object sender, RoutedEventArgs e) {
			//Debug.Print(threadTabs.SelectedIndex.ToString());
			if (threadTabs.SelectedIndex >= 0) {
				((threadTabs.Items.GetItemAt(threadTabs.SelectedIndex) as TabItem).Content as ThreadTab).CloseTab();
				threadTabs.Items.RemoveAt(threadTabs.SelectedIndex);
			}
		}

		private void diskBrowse_Click(object sender, RoutedEventArgs e) {
			var fd = new FolderBrowserDialog();
			fd.SelectedPath = Directory.GetCurrentDirectory();
			var result = fd.ShowDialog();
			if (result.Equals("OK")) {
				Debug.Print(fd.SelectedPath);
			}
			diskAddress.Text = fd.SelectedPath;
			Properties.Settings.Default.RootFolder = fd.SelectedPath;
			Properties.Settings.Default.Save();
		}

		private void ffmpegBrowse_Click(object sender, RoutedEventArgs e) {
			var fd = new FolderBrowserDialog();
			fd.SelectedPath = Properties.Settings.Default.FFMPEG;
			var result = fd.ShowDialog();
			if (result.Equals("OK")) {
				Debug.Print(fd.SelectedPath);
			}
			Properties.Settings.Default.FFMPEG = fd.SelectedPath;
			if (!File.Exists(Properties.Settings.Default.FFMPEG + "/ffmpeg.exe")) {
				Properties.Settings.Default.FFMPEG = "";
			}
			if (Properties.Settings.Default.FFMPEG != "") {
				FFMPEGLoaded = true;
			}
			ffmpegAddress.Text = Properties.Settings.Default.FFMPEG;
			Properties.Settings.Default.Save();
		}

		private void muteBox_UnChecked(object sender, RoutedEventArgs e) {
			mute = false;
			Properties.Settings.Default.Mute = mute;
			Properties.Settings.Default.Save();

			ImageViewer.UpdateVolume(null, null);
		}

		private void muteBox_Checked(object sender, RoutedEventArgs e) {
			mute = true;
			Properties.Settings.Default.Mute = mute;
			Properties.Settings.Default.Save();

			ImageViewer.UpdateVolume(null, null);
		}

		private void volumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
			volume = (float) (sender as Slider).Value;
			Properties.Settings.Default.Volume = volume;
			Properties.Settings.Default.Save();

			ImageViewer.UpdateVolume(null, null);
		}
	}
}
