﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using HtmlAgilityPack;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace _4chanDL {
	/// <summary>
	/// Interaction logic for UserControl1.xaml
	/// </summary>
	public partial class ThreadTab : UserControl {

		public string boardAddress;
		public string boardTitle;
		public string threadTitle;

		public string date;
		public string rootFolder;
		public Boolean threadFailure = false;

		HtmlWeb web;
		HtmlNode doc;
		List<string> downloaded = new List<string>();
		public List<string> imagesList = new List<string>();
		WebClient wc;

		System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
		int seconds = 0;
		bool threadDownloading = false;

		public void CloseTab() {
			if (wc != null) {
				wc.CancelAsync();
			}
		}

		public ThreadTab(string boardAddress, string date, string rootFolder) {
			InitializeComponent();
			this.boardAddress = boardAddress;
			if (!boardAddress.Contains("4chan.org")) {
				failThread("This isnt 4chan.  Who am I, where AM I!!!");
				return;
			}

			if (!boardAddress.Contains("/thread/")) {
				failThread("This isnt a thread.");
				return;
			}

			try {
				boardTitle = boardAddress.Split('/')[3];
			} catch {
				failThread("This address does not go to a 4chan board");
				return;
			}
			web = new HtmlWeb();
			doc = web.Load(boardAddress).DocumentNode;
			var nodes = doc.SelectNodes("//span[contains(@class, 'subject')]");

			if (nodes == null || nodes.Count == 0) {
				failThread("Page contains no thread");
				return;
			}
			StringBuilder titleString = new StringBuilder(nodes[1].InnerText);
			for (int i = 0; i < titleString.Length; i++) {
				char c = titleString[i];
				if (c == '<' || c == '>' || c == ':' || c == '/' || c == '\\' || c == '|' || c == '?' || c == '*' || c == '.') {
					titleString[i] = ' ';
				}
			}
			threadTitle = titleString.ToString();
			foreach (char c in System.IO.Path.GetInvalidPathChars()) {
				threadTitle = threadTitle.Replace(c.ToString(), "");
			}
			foreach (char c in System.IO.Path.GetInvalidFileNameChars()) {
				threadTitle = threadTitle.Replace(c.ToString(), "");
			}
			title.Text = threadTitle.Replace(",", "").Trim();

			this.date = date;
			this.rootFolder = rootFolder;
			//Debug.Print("Address : " + boardAddress);
			//Debug.Print("Name : " + threadTitle);
			//Debug.Print("Board : " + boardTitle);

			timer.Interval = 1000;
			timer.Tick += (sen, arg) => {
				if (!threadDownloading) {
					seconds += 1;
					downloadButton.Content = "Refresh (" + (120 - seconds) + ")";
					if (seconds >= 120) {
						seconds = 0; downloadThreadAsync();
					}
				} else {
					seconds = 0;
					downloadButton.Content = "Downloading";
				}
			};
		}

		private void failThread(string message) {
			errorText.Text = message;
			threadFailure = true;
			downloadButton.IsEnabled = false;
		}

		async void downloadThreadAsync() {
			downloadButton.IsEnabled = false;
			downloadButton.Background = new SolidColorBrush(Color.FromRgb(0, 255, 0));
			try {
				threadDownloading = true;
				if (boardAddress != "") {
					doc = web.Load(boardAddress).DocumentNode;

					//Debug.Print(Directory.GetCurrentDirectory().ToString());
					var nodes = doc.SelectNodes("//div[contains(@class, 'fileText')]");

					string path = rootFolder + "/" + date + "/" + boardTitle + "/" + threadTitle + "/";

					int images = nodes.Count;
					progressText.Text = (imagesList.Count + "/" + images);
					progressBar.Value = ((float)(imagesList.Count) / images) * 100;

					foreach (var node in nodes) {
						//Debug.Print(node.InnerHtml);
						string filePath = node.ChildNodes[1].InnerText;
						if (node.ChildNodes[1].Attributes["title"] != null) {
							filePath = node.ChildNodes[1].Attributes["title"].Value;
						}
						filePath.Trim();
						string fileURL = "https:" + node.ChildNodes[1].Attributes["href"].Value.ToString();
						//Debug.Print(filePath + " : " + fileURL);
						Debug.Print(fileURL);
						if (!Directory.Exists(path)) {
							Directory.CreateDirectory(path);
						}
						if (!downloaded.Contains(filePath)) {
							bool download = true;
							wc = new WebClient();
							wc.DownloadFileCompleted += (s, file) => {
								if (file.Cancelled || file.Error != null) {
									string fileName = path + filePath;
									File.Delete(path + filePath);
								}
								download = false;
							};

							if (!File.Exists(path + filePath)) {
								await wc.DownloadFileTaskAsync(new Uri(fileURL), path + filePath);
								while (download) {

								}
								//Debug.Print("	Image Gotten");
							}

							FrameworkElement element = null;
							Debug.Print(fileURL);

							if (fileURL.EndsWith(".png") || fileURL.EndsWith(".jpg") || fileURL.EndsWith(".jpeg") || fileURL.EndsWith(".gif")) {
								Image i = new Image();
								i.Stretch = Stretch.Uniform;
								var bi = new BitmapImage();
								bi.BeginInit();
								bi.UriSource = new Uri(path + filePath);
								bi.DecodePixelHeight = 200;
								bi.EndInit();
								i.Source = bi;
								element = i;
							} else if (fileURL.EndsWith(".webm")) {
								Image i = new Image();
								i.Stretch = Stretch.Uniform;
								var bi = new BitmapImage();
								bi.BeginInit();
								bi.UriSource = new Uri("pack://application:,,,/Images/Video.png");
								bi.DecodePixelHeight = 200;
								bi.EndInit();
								i.Source = bi;
								element = i;
							} else if (fileURL.EndsWith(".gif")) {
								MediaElement m = new MediaElement();
								m.LoadedBehavior = MediaState.Play;
								m.MediaEnded += (s, e) => { m.Position = TimeSpan.FromMilliseconds(1); };
								m.Source = new Uri(path + filePath);
								m.Stretch = Stretch.Uniform;
								element = m;
							} else {
								//Debug.Print("File ends with :" + filePath.Split('.')[filePath.Split('.').Length - 1]);
							}

							int imagesGotten = imagesList.Count;
							progressText.Text = ((imagesGotten + 1) + "/" + images);
							progressBar.Value = ((float)(imagesGotten + 1) / images) * 100;
							if ((imagesGotten) % 5 == 0) {
								var row = new RowDefinition();
								row.MaxHeight = 200;
								imageGrid.RowDefinitions.Add(row);
							}
							if (element == null) {
								element = new Image();
								Debug.Print(fileURL);
							}
							
							element.MouseLeftButtonUp += (s, args) => {
								//Debug.Print("Clicked on " + filePath);
								new ImageViewer(path + filePath, this).Show();
							};

							imageGrid.Children.Add(element);
							element.MaxHeight = 200;

							Canvas.SetTop(element, 0);
							Canvas.SetLeft(element, 0);
							Grid.SetColumn(element, (imagesGotten) % 5);
							Grid.SetRow(element, (imagesGotten) / 5);

							downloaded.Add(filePath);
							imagesList.Add(path + filePath);
							imagesGotten += 1;
						}
					}
				}
				threadDownloading = false;
			} catch (WebException web) {
				if (web.Status != WebExceptionStatus.RequestCanceled) {
					//MessageBox.Show("Web Response : " + web.Response + "\n" + "Web Status : " + web.Status);
					downloadButton.Content = "Thread 404";
					downloadButton.IsEnabled = false;
					downloadButton.Background = new SolidColorBrush(Color.FromRgb(255, 0, 0));
					MessageBox.Show("Web Response : " + web.Response + "\n" + "Web Status : " + web.Status);
					return;
				}
			}
			downloadButton.IsEnabled = true;
			downloadButton.Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
		}

		public void downloadButton_Click(object sender, RoutedEventArgs e) {
			downloadButton.Content = "Refreshing";
			seconds = 0;
			downloadThreadAsync();
			downloadButton.Content = "Refresh (120)";
			timer.Start();
		}

		private void title_TextChanged(object sender, TextChangedEventArgs e) {
			threadTitle = title.Text;
			downloaded.Clear();
		}
	}
}
