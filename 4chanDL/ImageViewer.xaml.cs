﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Windows.Threading;

namespace _4chanDL
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class ImageViewer : Window{
		public static FrameworkElement element;
		ThreadTab tab;
		int index;
		float zoom = 1.0f;

		Point first = new Point();
		Point center = new Point();
		Point offset = new Point();
		
        public ImageViewer(string imagePath, ThreadTab tab)
        {
			this.tab = tab;
            InitializeComponent();

			SetImage(imagePath);
			parent.MouseUp += Image_MouseUp;
			parent.MouseDown += Image_MouseDown;
			parent.MouseMove += Image_MouseMove;
			parent.LayoutUpdated += Parent_LayoutUpdated;
			parent.MouseWheel += ImageViewer_MouseWheel;

			this.Closing += ImageViewer_Closing;
			
			for (int i = 0; i < tab.imagesList.Count; i++) {
				if (tab.imagesList[i].Equals(imagePath)) {
					index = i;
				}
			}
			updateImage();

			MouseMove += UpdateVolume;

			DispatcherTimer timer = new DispatcherTimer();
			timer.Interval = TimeSpan.FromSeconds(1f / 30f);
			timer.Tick += timer_Tick;
			timer.Start();

			
        }

		private void timer_Tick(object sender, EventArgs e) {
			if (element is Unosquare.FFME.MediaElement) {
				VideoInfo.Visibility = Visibility.Visible;
				float playbackProgress = 0;
				if (element is MediaElement) {
					if ((element as MediaElement).NaturalDuration.HasTimeSpan) {
						playbackProgress = (float)((element as MediaElement).Position.TotalMilliseconds / (element as MediaElement).NaturalDuration.TimeSpan.TotalMilliseconds);
						EndTime.Text = (element as MediaElement).NaturalDuration.TimeSpan.ToString().Split('.')[0];
					}
				}

				if (element is Unosquare.FFME.MediaElement) {
					if ((element as Unosquare.FFME.MediaElement).NaturalDuration.HasTimeSpan) {
						playbackProgress = (float)((element as Unosquare.FFME.MediaElement).Position.TotalMilliseconds / (element as Unosquare.FFME.MediaElement).NaturalDuration.TimeSpan.TotalMilliseconds);
						EndTime.Text = (element as Unosquare.FFME.MediaElement).NaturalDuration.TimeSpan.ToString().Split('.')[0];
					}
				}

				if (canvas.ActualWidth > 0) {
					Canvas.SetLeft(EndTime, canvas.ActualWidth - 60);
					ProgressBar.Width = canvas.ActualWidth - 80;
					Canvas.SetLeft(ProgressBar, 10);
					Canvas.SetLeft(ProgressBlip, 10 + ProgressBar.ActualWidth * playbackProgress);
				}
			} else {
				VideoInfo.Visibility = Visibility.Collapsed;
			}
		}

		public static void UpdateVolume(object sender, MouseEventArgs e) {
			if (element != null) {
				if (element is Unosquare.FFME.MediaElement) {
					(element as Unosquare.FFME.MediaElement).IsMuted = MainWindow.mute;
					(element as Unosquare.FFME.MediaElement).Volume = MainWindow.volume;
				}
			}
		}

		private void ImageViewer_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
			if (element is MediaElement) {
				(element as MediaElement).Source = null;
			}
			if (element is Unosquare.FFME.MediaElement) {
				(element as Unosquare.FFME.MediaElement).Source = null;
			}
			element = null;

			parent.MouseUp -= Image_MouseUp;
			parent.MouseDown -= Image_MouseDown;
			parent.MouseMove -= Image_MouseMove;
			parent.LayoutUpdated -= Parent_LayoutUpdated;
			parent.MouseWheel -= ImageViewer_MouseWheel;
		}

		private void ImageViewer_MouseWheel(object sender, MouseWheelEventArgs e) {
			offset.X /= zoom;
			offset.Y /= zoom;
			zoom += e.Delta * 0.0005f * zoom;
			offset.X *= zoom;
			offset.Y *= zoom;
			if (zoom < 0.10) {
				zoom = 0.10f;
			}
			if (zoom > 100) {
				zoom = 100;
			}
			updateImage();
		}

		private void Parent_LayoutUpdated(object sender, EventArgs e) {
			updateImage();
		}

		private void Image_SourceUpdated(object sender, DataTransferEventArgs e) {
			updateImage();
		}

		protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo) {
			base.OnRenderSizeChanged(sizeInfo);
			updateImage();
		}

		private void updateImage() {
			if (element != null) {
				if (canvas.ActualWidth > 0 && canvas.ActualHeight > 0) {
					element.Width = canvas.ActualWidth * zoom;
					if (element is Unosquare.FFME.MediaElement) {
						if (canvas.ActualHeight >= 22) {
							element.Height = ((float)(canvas.ActualHeight) - 22.0) * zoom;
							Canvas.SetTop(element, offset.Y + center.Y - 22);
						}
					} else {
						element.Height = ((float)(canvas.ActualHeight)) * zoom;
						Canvas.SetTop(element, offset.Y + center.Y);
					}
					center = new Point(canvas.ActualWidth / 2 - element.Width / 2, canvas.ActualHeight / 2 - element.Height / 2);
					Canvas.SetLeft(element, offset.X + center.X);
				}
				UpdateVolume(null, null);
				timer_Tick(null, null);
			}
		}

		private void Image_MouseMove(object sender, MouseEventArgs e) {
			if (e.MiddleButton == MouseButtonState.Pressed) {
				Point temp = e.GetPosition(this);
				Point r = new Point(temp.X - first.X, temp.Y - first.Y);
				offset = new Point(offset.X + r.X, offset.Y + r.Y);
				first = temp;
			}
			updateImage();
		}

		private void Image_MouseDown(object sender, MouseButtonEventArgs e) {
			if (e.ChangedButton == MouseButton.Middle) {
				first = e.GetPosition(this);
			}
			if (e.ChangedButton == MouseButton.Right) {
				zoom = 1;
				offset = new Point(0, 0);
			}
			updateImage();
			UpdateVolume(null, null);
		}

		private void Image_MouseUp(object sender, MouseButtonEventArgs e) {
			if (e.ChangedButton == MouseButton.Left) {
				try {
					Point p = e.GetPosition(sender as Window);
					double x = p.X - parent.ActualWidth / 2;
					double y = p.Y - parent.ActualHeight / 2;
					//Debug.Print(x + "," + y);


					if (x <= 0) {
						index -= 1;
					}

					if (x >= 0) {
						index += 1;
					}

					if (index < 0) {
						index += tab.imagesList.Count;
					}

					if (index >= tab.imagesList.Count) {
						index = 0;
					}

					//Debug.Print(index + " : " + tab.imagesList.Count);
					string fileURL = tab.imagesList[index];

					SetImage(fileURL);
					offset = new Point();
				} catch {

				}
			}
			updateImage();
			UpdateVolume(null, null);
		}

		private void SetImage(string fileURL) {
			try {
				zoom = 1.0f;

				first = new Point();
				center = new Point();
				offset = new Point();
				if (MainWindow.FFMPEGLoaded) {
					Unosquare.FFME.MediaElement.FFmpegDirectory = Properties.Settings.Default.FFMPEG;
				}

				if (element is MediaElement) {
					(element as MediaElement).Source = null;
				}
				if (element is Unosquare.FFME.MediaElement) {
					(element as Unosquare.FFME.MediaElement).Source = null;
				}
				canvas.Children.Clear();
				if (fileURL.EndsWith(".png") || fileURL.EndsWith(".jpg") || fileURL.EndsWith(".jpeg")) {
					BitmapImage bm;
					bm = new BitmapImage(new Uri(fileURL));
					Image i = new Image();
					i.Source = bm;
					element = i;
				} else if (fileURL.EndsWith(".gif")) {
					MediaElement m = new MediaElement();
					m.LoadedBehavior = MediaState.Play;
					m.MediaEnded += (s, e) => { m.Position = TimeSpan.FromMilliseconds(0); };
					m.Source = new Uri(fileURL);
					m.Stretch = Stretch.Uniform;
					element = m;

				} else if (fileURL.EndsWith(".webm")) {
					if (MainWindow.FFMPEGLoaded) {
						Unosquare.FFME.MediaElement m = new Unosquare.FFME.MediaElement();
						m.LoadedBehavior = MediaState.Play;
						m.MouseEnter += (s, e) => { Debug.Print(m.Position.ToString()); };
						m.MediaEnded += (s, e) => { m.Position = TimeSpan.FromSeconds(0); m.Play(); };
						m.Source = new Uri(fileURL);
						m.Stretch = Stretch.Uniform;
						element = m;

						UpdateVolume(null, null);
					} else {
						BitmapImage bm;
						bm = new BitmapImage(new Uri("pack://application:,,,/Images/Video.png"));
						Image i = new Image();
						i.Source = bm;
						element = i;
					}
				} else {
					BitmapImage bm;
					bm = new BitmapImage(new Uri("pack://application:,,,/Images/CantDoIt.png"));
					Image i = new Image();
					i.Source = bm;
					element = i;
				}

				UpdateVolume(null, null);

				canvas.Children.Add(element);
				string[] fileName = fileURL.Split('/');
				this.Title = (index + 1) + " / " + tab.imagesList.Count + "   " + fileName[fileName.Length - 1];
			} catch (Exception e) {
				MessageBox.Show(e.ToString());
			}
		}
	}
}
